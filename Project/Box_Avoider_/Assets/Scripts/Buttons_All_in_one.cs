﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Buttons_All_in_one : MonoBehaviour {

    public GameObject player, menu, pausa, pauseMenu, RestartMenu, shop, Score, MaxScore, curmenu, Music,SkinsMenu;
    public Transform playertrans;
    int prize;
    public static bool casinobutt; public static int MusicOn = 1;
    float lastpress;
    public Text CoinText;
    public Sprite twentyfive, fity, onehun, twohun, fivehun, onefive, downsprite, upsprite, MusicOffDown, MusicOffUp, MusicOnDown, MusicOnUp;
    public SpriteRenderer prizevar;

    private void Start()
    {
        MusicOn = PlayerPrefs.GetInt("MusicOn");
        if (gameObject.tag == "Switch")
        {
            if (MusicOn != 0)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = MusicOnUp;
                downsprite = MusicOnDown;
                upsprite = MusicOnUp;
            }
            else
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = MusicOffUp;
                downsprite = MusicOffDown;
                upsprite = MusicOffUp;
            }
        }
        casinobutt = false;
        lastpress = 0;
    }

    private void OnMouseDown()
    {
        gameObject.GetComponent<SpriteRenderer>().sprite = downsprite;
        if (MusicOn != 0)
            GameObject.Find("Camera").GetComponent<AudioSource>().Play();
    }

    private void OnMouseExit()
    {
        gameObject.GetComponent<SpriteRenderer>().sprite = upsprite;
    }



    private void OnMouseUpAsButton()
    {
        gameObject.GetComponent<SpriteRenderer>().sprite = upsprite;
        switch (gameObject.tag)
        {
			case "return1":
				SkinsMenu.SetActive (false);
				menu.SetActive (true);
			break;
			case "accept":
				SkinsMenu.GetComponent<Skins> ().Accept ();
			break;
			case "bckwrd":
				SkinsMenu.GetComponent<Skins> ().ChangeSprite (false);
			break;
			case "frwrd":
				SkinsMenu.GetComponent<Skins> ().ChangeSprite (true);
			break;
			case "Skins":
				menu.SetActive (false);
				SkinsMenu.GetComponent<Skins>().Enable();
			break;
            case "restart":
                if (MusicOn != 0)
                    GameObject.Find("BackgroundMusic").GetComponent<BGMPlayer>().canPlay=true;
                	playertrans.position = new Vector3(-1.5f, 1);
                	player.SetActive(true);
                	Player_Mover.restart = true;
                	RestartMenu.SetActive(false);
               	 pausa.SetActive(true);
                break;
            case "play":
                MaxScore.SetActive(false);
                if (MusicOn != 0)
                {
                    GameObject.Find("MenuMusic").GetComponent<AudioSource>().Stop();
                    GameObject.Find("BackgroundMusic").GetComponent<BGMPlayer>().Play();
                }
                player.SetActive(true);
                pausa.SetActive(true);
                menu.SetActive(false);
                break;
            case "pause":
                Time.timeScale = 0;
                Mover_for_boxes.run = false;
                pauseMenu.SetActive(true);
                gameObject.SetActive(false);
                if (MusicOn != 0)
                    GameObject.Find("BackgroundMusic").GetComponent<BGMPlayer>().Pause();
                break;
            case "pauserestart":
                Player_Mover.lose = true;
                playertrans.position = new Vector3(-1.5f, 1);
                player.SetActive(false);
                player.SetActive(true);
                if (MusicOn != 0)
                {
                    GameObject.Find("BackgroundMusic").GetComponent<BGMPlayer>().Stop();
                    GameObject.Find("BackgroundMusic").GetComponent<BGMPlayer>().Play();
                }
                Player_Mover.restart = true;
                pausa.SetActive(true);
                Time.timeScale = 1;
                pauseMenu.SetActive(false);
                break;
            case "resume":
                if (MusicOn != 0)
                    GameObject.Find("BackgroundMusic").GetComponent<BGMPlayer>().Play();
                pausa.SetActive(true);
                pauseMenu.SetActive(false);
                Mover_for_boxes.run = true;
                Time.timeScale = 1;
                break;
            case "exit":

                Application.Quit();

                break;
            case "menu":
                SceneManager.LoadScene("GamePlay");
                break;
            case "shop":
                menu.SetActive(false);
                shop.SetActive(true);
                Score.SetActive(false);
                MaxScore.SetActive(false);
                break;
           
                if (MusicOn != 0)
                {
                    GameObject.Find("CasinoMusic").GetComponent<AudioSource>().Play();
                    GameObject.Find("MenuMusic").GetComponent<AudioSource>().volume = 0.1f;
                }
                casinobutt = true;

                break;
            case "savemenu":
                curmenu.SetActive(false);
                menu.SetActive(true);
                Score.SetActive(true);
                MaxScore.SetActive(true);
                break;
            case "Switch":
                if (MusicOn == 1)
                {
                    MusicOn = 0;
                    Music.SetActive(false);
                    PlayerPrefs.SetInt("MusicOn", MusicOn);
                    downsprite = MusicOffDown;
                    upsprite = MusicOffUp;
                }
                else
                {
                    MusicOn = 1;
                    Music.SetActive(true);
                    GameObject.Find("MenuMusic").GetComponent<AudioSource>().Play();
                    PlayerPrefs.SetInt("MusicOn", MusicOn);
                    downsprite = MusicOnDown;
                    upsprite = MusicOnUp;
                }
                break;
            case "Reload":
                SceneManager.LoadScene("GamePlay");
                break;

        }
        gameObject.GetComponent<SpriteRenderer>().sprite = upsprite;
    }




    public void Buy()
    {
        
    }
}

