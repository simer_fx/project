﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]

public class BGMPlayer : MonoBehaviour {

	public AudioClip[] BGMClip;
	AudioSource BGMusic;
	int BGMTrack;
	public bool canPlay = false;
	bool next = true;
	int step;

	void Start()
	{
		BGMusic = GetComponent<AudioSource> ();
		BGMTrack = Random.Range (0, BGMClip.Length); 
		BGMusic.clip = BGMClip[BGMTrack];
	}
		
	public void Play () 
	{
		canPlay = true;
		BGMusic.Play ();	
	}

	public void Pause() 
	{
		canPlay = false;
		BGMusic.Pause ();
	}

	public void Stop()
	{
		canPlay = false;
		BGMusic.Stop ();
	}

	public void OtherTrack()
	{
		next = (Random.value > 0.5f);
		step = (Random.Range (0, (BGMClip.Length-1)));
		if (step > ((BGMClip.Length - 1) - BGMTrack)) 
		{
			step=step-((BGMClip.Length - 1) - BGMTrack);
		}

		if (next == true) {
			BGMTrack=BGMTrack+step; 
			if (BGMTrack > (BGMClip.Length - 1)) {
				BGMTrack = 0;
			}
		} 
		else 
		{
			BGMTrack=BGMTrack-step;;
			if (BGMTrack < (0)) 
			{
				BGMTrack = BGMClip.Length - 1;
			}

		}

		
	}

	void Update () 
	{
		if ((BGMusic.isPlaying != true) && (canPlay == true))
		{
			OtherTrack ();
			BGMusic.clip = BGMClip[BGMTrack];
			Play ();
		}
		
	}
}
