﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Skins : MonoBehaviour {

	public Sprite[] skinsArray;
	public Sprite[] bckgrndArray;
	public GameObject bckgrnd;
	int activeSkin = 0;

	public void Enable()
	{
		gameObject.SetActive (true);
		gameObject.transform.GetChild (0).GetChild (0).GetComponent<Image> ().sprite = skinsArray [activeSkin];

	}

	public void ChangeSprite(bool isForward)
	{
		if (isForward) 
		{
			activeSkin++;
		} 
		else 
		{
			activeSkin--;
		}

		if (activeSkin < 0) 
		{
			activeSkin = skinsArray.Length-1;
		}
		if(activeSkin > skinsArray.Length-1)
		{
			activeSkin = 0;
		}

		gameObject.transform.GetChild (0).GetChild (0).GetComponent<Image> ().sprite = skinsArray [activeSkin];

	}

	public void Accept()
	{
		//bckgrnd.GetComponent<SpriteRenderer> ().sprite = bckgrndArray[activeSkin];
	}

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
