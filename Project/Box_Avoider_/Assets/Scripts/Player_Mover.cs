﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player_Mover : MonoBehaviour {

    public float JumpForce;
    private float maxscore;
    public static bool lose = true , restart = false;
    private Rigidbody2D _rg;
    public bool Ground;
    private int Coin;
    public LayerMask WhatIsGround;
    private Collider2D _Cl;
    public static float Score;
    public Text scoreText, Cointext;
    private float timeStamp;
    public GameObject RestartMenu, pausa;

    // for box spawn
    public  GameObject Box;
    public int BoxCount = 10;
    public float spawnWait, WaitMin = 0.8f, WaitMax = 2f;
    //
    void Start() {
      //  GameObject.Find("MaxScore").GetComponent<Text>().text = "Max Score:" + PlayerPrefs.GetFloat("Player Score", Score).ToString();
      //  Cointext = GameObject.Find("CoinText").GetComponent<Text>();
        lose = false;
        _rg = GetComponent<Rigidbody2D>();
        _Cl = GetComponent<Collider2D>();
        LoadSave();
        StartCoroutine(SpawnBox());
        Score = 0;
        StartCoroutine(PlusScore());
     // Coin = PlayerPrefs.GetInt("CoinCount", Coin);
       // Cointext.text = PlayerPrefs.GetInt("CoinCount", Coin) + "";
    }
    public void LoadSave()
    {
        
        PlayerPrefs.GetInt("CoinCount", Coin);

    }

    //private void OnTriggerEnter2D(Collider2D other)
    //{
        //if (other.gameObject.tag == "Coin")
        //{
            //Coin++;
            //Destroy(other.gameObject);
            //PlayerPrefs.SetInt("CoinCount", PlayerPrefs.GetInt("CoinCount") + 1);
        //}
    //}

    void Update()
    {
		Coin=((int) Score)/10;
        Ground = Physics2D.IsTouchingLayers(_Cl, WhatIsGround);
        _rg.velocity = new Vector2(0, _rg.velocity.y);

        if (restart)
        {
            Coin = 0;
            lose = false;
            StartCoroutine(PlusScore());
            StartCoroutine(SpawnBox());
            LoadSave();
            Score = 0;
            restart = false;
        }

        scoreText.text = "Score:" + Score;
        Cointext.text = Coin.ToString();
        // If player mertvuy
        if (gameObject.transform.position.y < -5)
        {
            if (Buttons_All_in_one.MusicOn != 0)
            {
				GameObject.Find("BackgroundMusic").GetComponent<BGMPlayer>().Stop();
                GameObject.Find("DeathMusic").GetComponent<AudioSource>().Play();
            }
			PlayerPrefs.SetInt ("CoinCount", PlayerPrefs.GetInt ("CoinCount") + Coin );
            RestartMenu.SetActive(true);
            gameObject.SetActive(false);
            pausa.SetActive(false);
            lose = true;
        }
          if (Input.GetMouseButtonDown(0))
          {
              if (Ground)
              {
                if (Buttons_All_in_one.MusicOn != 0)
                    GameObject.Find("JumpMusic").GetComponent<AudioSource>().Play();
                  _rg.velocity = new Vector2(_rg.velocity.x, JumpForce);
              }


          }
          if (lose&& PlayerPrefs.GetFloat("Player Score") < Score) //визначаемо бест скор
            PlayerPrefs.SetFloat("Player Score", Score);
    }

     IEnumerator PlusScore()
    {
        while (!lose)
        {
            Score += 1;
            yield return new WaitForSeconds(1.5f);
        }
    }
    // SPAWN BOXES
    IEnumerator SpawnBox()
    {
        while (!lose)
        {
            for (int i = 0; i < BoxCount; i++)
            {
                spawnWait = Random.Range(WaitMin, WaitMax);
                Instantiate(Box, new Vector3(3.8f, -2.5f), Quaternion.identity);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(3f); //wave wait
        }
    }
}
