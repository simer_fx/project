﻿using UnityEngine.UI;
using UnityEngine;

public class OnStart : MonoBehaviour {

    public static string SkinType = "default";
    public GameObject Player, Background;
    //Boxes
    public GameObject DefaultBox, StarWarsBox, PacmanBox;
    //Backgrounds
    public Sprite DefaultBack, StarWarsBack, PacmanBack;

	// Use this for initialization
	void Start () {
        SkinType = PlayerPrefs.GetString("SkinParameter");
        GameObject.Find("MaxScore").GetComponent<Text>().text = "Max Score:" + PlayerPrefs.GetFloat("Player Score").ToString();
        GameObject.Find("CoinText").GetComponent<Text>().text = PlayerPrefs.GetInt("CoinCount") + "";
        Time.timeScale = 1;
        GameObject.Find("MenuMusic").GetComponent<AudioSource>().Stop();
        if (Buttons_All_in_one.MusicOn != 0)
            GameObject.Find("MenuMusic").GetComponent<AudioSource>().Play();
        //Присвовуемо спрайти выдповидно до скинив
       
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
