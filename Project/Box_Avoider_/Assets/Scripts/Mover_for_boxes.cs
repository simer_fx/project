﻿using UnityEngine;

public class Mover_for_boxes : MonoBehaviour {
    public float Speed;
    private Rigidbody2D _rg;
    public float force = 3;
    public static bool run;

    // Use this for initialization
    void Start ()
    {
		float randomValue = Random.Range(0.13f, 0.13f); // Розмір коробок
        transform.localScale = new Vector3(randomValue, randomValue, 1);
        run = true;
        _rg = GetComponent<Rigidbody2D>();
		//GameObject.Find("Player(Stickman)")GetComponent<Player_Mover>().Score;
		//Player_Mover.Score
	}
    void OnCollisionEnter(Collision c)
    {
        if (c.gameObject.tag == "Player")
        {

            Vector3 dir = c.contacts[0].point - transform.position;

            dir = -dir.normalized;


            GetComponent<Rigidbody>().AddForce(dir * force);
        }
    }
    // Update is called once per frame
	void Update ()
	{
		//Speed += Random.Range(0.05f, 0.05f); // Швидкість коробок

		Speed = (Player_Mover.Score/10)+5;

		if (Player_Mover.restart)
			Destroy(gameObject);
		if (run)
			_rg.AddForce((Vector2.left) * Speed);
	}

}
