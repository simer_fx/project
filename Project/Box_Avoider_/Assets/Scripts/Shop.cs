﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Shop : MonoBehaviour
{
    public Button GetSkinButton;
    public Sprite[] Skins;
    public bool[] SkinAvaliability;
    private const int SKIN_COUNT = 3;
    public Image SelectedSkin;
    private int _openedSkins;

    private void Start()
    {
        PlayerPrefs.DeleteAll();
        SkinAvaliability = new bool[SKIN_COUNT];
        if (PlayerPrefs.HasKey("SkinAvialbleCount"))
        {
            _openedSkins = PlayerPrefs.GetInt("SkinAvialbleCount", 0);
            if(_openedSkins == SKIN_COUNT-1)
            {
                GetSkinButton.interactable = false;
                return;
            }
            
            for(int index = 0; index < SKIN_COUNT; index++)
            {
                if(PlayerPrefs.HasKey("Skin_" + index))//Skin_0 Skin_1
                {
                    SkinAvaliability[index] = true;
                }
            }
        }
    }
    public void GetSkin()
    {
        bool finish = false;
        int aval = 0;
        foreach(var skin in SkinAvaliability)
        {
            if(skin)
            {
                aval++;
            }
        }
        if(aval == SKIN_COUNT)
        {
            return;
        }
        do
        {
            int randIndex = Random.Range(0, SKIN_COUNT);
            if(SkinAvaliability[randIndex] == false)
            {
                //PlayerPrefs.GetInt
                //SkinSelector.GlobalSkinAvaiability[randIndex] = true;
                SelectedSkin.sprite = Skins[randIndex];
                SkinAvaliability[randIndex] = true;
                PlayerPrefs.SetInt("Skin_" + randIndex, 1);
                int current = PlayerPrefs.GetInt("SkinAvialbleCount", 0);
                PlayerPrefs.SetInt("SkinAvialbleCount", ++current);
                finish = true;
            }
        }
        while (!finish);
    }
}